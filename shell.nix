with import <nixpkgs> {};

mkShell rec {
  name = "glfw-dev";

  buildInputs = [] ++ lib.optionals stdenv.isLinux [
    xorg.libX11
    vulkan-loader
    libxkbcommon
    libGL
  ];

  LD_LIBRARY_PATH = lib.makeLibraryPath buildInputs;
}
