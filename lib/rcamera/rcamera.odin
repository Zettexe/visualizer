package rcamera

import raylib "vendor:raylib"
foreign import "rcamera.a"

import _c "core:c"

@(default_calling_convention="c")
foreign rcamera {

    @(link_name="GetCameraForward")
    GetCameraForward :: proc(camera : ^raylib.Camera) -> raylib.Vector3 ---;

    @(link_name="GetCameraUp")
    GetCameraUp :: proc(camera : ^raylib.Camera) -> raylib.Vector3 ---;

    @(link_name="GetCameraRight")
    GetCameraRight :: proc(camera : ^raylib.Camera) -> raylib.Vector3 ---;

    @(link_name="CameraMoveForward")
    CameraMoveForward :: proc(camera : ^raylib.Camera, distance : _c.float, moveInWorldPlane : bool) ---;

    @(link_name="CameraMoveUp")
    CameraMoveUp :: proc(camera : ^raylib.Camera, distance : _c.float) ---;

    @(link_name="CameraMoveRight")
    CameraMoveRight :: proc(camera : ^raylib.Camera, distance : _c.float, moveInWorldPlane : bool) ---;

    @(link_name="CameraMoveToTarget")
    CameraMoveToTarget :: proc(camera : ^raylib.Camera, delta : _c.float) ---;

    @(link_name="CameraYaw")
    CameraYaw :: proc(camera : ^raylib.Camera, angle : _c.float, rotateAroundTarget : bool) ---;

    @(link_name="CameraPitch")
    CameraPitch :: proc(camera : ^raylib.Camera, angle : _c.float, lockView : bool, rotateAroundTarget : bool, rotateUp : bool) ---;

    @(link_name="CameraRoll")
    CameraRoll :: proc(camera : ^raylib.Camera, angle : _c.float) ---;

    @(link_name="GetCameraViewMatrix")
    GetCameraViewMatrix :: proc(camera : ^raylib.Camera) -> raylib.Matrix ---;

    @(link_name="GetCameraProjectionMatrix")
    GetCameraProjectionMatrix :: proc(camera : ^raylib.Camera, aspect : _c.float) -> raylib.Matrix ---;

}
