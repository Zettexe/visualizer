package main

import "core:c"
import "core:fmt"
import "core:math/linalg"
import rc "lib/rcamera"
import rl "vendor:raylib"

import "core:math"

Infinite :: 1e17

main :: proc() {
	rl.InitWindow(1280, 720, "Raylib Test")
	defer rl.CloseWindow()

	rl.SetTargetFPS(60)

	camera: rl.Camera = {}
	camera.position = rl.Vector3{0.0, 0.0, 4.0} // Camera position
	camera.target = rl.Vector3{0.0, 0.0, 0.0} // Camera looking at point
	camera.up = rl.Vector3{0.0, 1.0, 0.0} // Camera up vector (rotation towards target)
	camera.projection = rl.CameraProjection.ORTHOGRAPHIC
	camera.fovy = 5.0 // near plane width in CAMERA_ORTHOGRAPHIC
	cameraYawTarget: c.float = 45 * rl.DEG2RAD
	cameraYaw: c.float = cameraYawTarget
	rc.CameraYaw(&camera, cameraYaw, true)
	rc.CameraPitch(&camera, -45 * rl.DEG2RAD, true, true, false)

	fmt.println("Raylib Initialized")

	model := rl.LoadModel("ExampleTile.m3d")
	fmt.println("Meshes: ", model.meshCount)

	for !rl.WindowShouldClose() {
		delta := rl.GetFrameTime()

		if rl.IsKeyPressed(rl.KeyboardKey.LEFT_SHIFT) {

			if rl.IsKeyPressed(rl.KeyboardKey.Q) {
				cameraYawTarget -= 90 * rl.DEG2RAD
				//  fmt.println("Target: ", cameraYawTarget)
			}
			if rl.IsKeyPressed(rl.KeyboardKey.E) {
				cameraYawTarget += 90 * rl.DEG2RAD
				//  fmt.println("Target: ", cameraYawTarget)
			}
		}

		cameraTurnSpeed := 5 * delta
		if !IsCloseEnough(cameraYaw, cameraYawTarget) {
			targetDirection := TargetDirection(cameraYaw, cameraYawTarget)
			cameraYaw += cameraTurnSpeed * targetDirection
			rc.CameraYaw(&camera, cameraTurnSpeed * targetDirection, true)
			//  fmt.println("Angle: ", cameraYaw)
		} else if cameraYaw != cameraYawTarget {
			cameraYaw = cameraYawTarget
			//  fmt.println("Set to target: ", cameraYaw)
		}

		rl.BeginDrawing()
		{
			rl.ClearBackground(rl.BLACK)
			rl.BeginMode3D(camera)
			{
				mousePosition := rl.GetMousePosition()
				//  fmt.printfln("x: %v, y: %v", mousePosition.x, mousePosition.y)
				mouseRay := rl.GetMouseRay(mousePosition, camera)
				collision := GetRayCollisionInfPlane(mouseRay, 0)
				if collision.hit {
					rl.DrawModel(model, collision.point, 1, rl.BLUE)
					rl.DrawModelWires(model, collision.point, 1, rl.BLACK)
				}
				rl.DrawModel(model, {0, 0, 0}, 1, rl.WHITE)
				rl.DrawModelWires(model, {0, 0, 0}, 1, rl.BLACK)
			}
			rl.EndMode3D()
		}
		rl.EndDrawing()
	}
}

IsCloseEnough :: proc(origin: c.float, target: c.float) -> bool {
	return origin <= target + 2 * rl.DEG2RAD && origin >= target - 2 * rl.DEG2RAD
}

TargetDirection :: proc(origin, target: c.float) -> c.float {
	return math.copy_sign(1.0, target - origin)
}

GetRayCollisionInfPlane :: proc(ray: rl.Ray, height: c.float) -> rl.RayCollision {
	//  ( 1, 1) ( 1, -1)
	//  (-1, 1) (-1, -1)
	return rl.GetRayCollisionQuad(
		ray,
		{-Infinite, height, Infinite},
		{Infinite, height, Infinite},
		{Infinite, height, -Infinite},
		rl.Vector3{-Infinite, height, -Infinite},
	)
}
